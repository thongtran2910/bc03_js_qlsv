function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSv = document.getElementById("txtEmail").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemLy = document.getElementById("txtDiemLy").value * 1;
  var diemHoa = document.getElementById("txtDiemHoa").value * 1;

  var sinhVien = new SinhVien(maSv, tenSv, emailSv, diemToan, diemLy, diemHoa);
  return sinhVien;
}

function xuatDanhSachSinhVien(dssv) {
  var contentHTML = "";
  for (var index = 0; index < dssv.length; index++) {
    var sinhVien = dssv[index];
    var contentTrTag = `<div><tr>
      <td>${sinhVien.maSv}</td>
      <td id="nameTag">${sinhVien.tenSv}</td>
      <td>${sinhVien.emailSv}</td>
      <td>${sinhVien.diemTB}</td>
      <td>
        <button onclick="suaSinhVien('${sinhVien.maSv}')" class="btn btn-success">Sửa</button>
        <button onclick="xoaSinhVien('${sinhVien.maSv}')" class="btn btn-danger">Xóa</button>
      </td>
      </tr></div>`;
    contentHTML += contentTrTag;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xuatThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSv;
  document.getElementById("txtTenSV").value = sv.tenSv;
  document.getElementById("txtEmail").value = sv.emailSv;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
