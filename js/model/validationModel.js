function ValidatorSV() {
  this.kiemTraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  this.kiemTraIdHopLe = function (newSinhVien, danhSachSinhVien) {
    var index = danhSachSinhVien.findIndex(function (item) {
      return item.maSv == newSinhVien.maSv;
    });
    if (index == -1) {
      document.getElementById("spanMaSV").innerText = "";
      return true;
    } else {
      document.getElementById("spanMaSV").innerText =
        "Mã sinh viên không được trùng";
      return false;
    }
  };
  this.kiemTraEmail = function (idTarget, idError) {
    var pattern =
      /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Email không hợp lệ";
      return false;
    }
  };
}
