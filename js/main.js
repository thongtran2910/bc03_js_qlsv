var danhSachSinhVien = [];
var validatorSv = new ValidatorSV();
const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSv == id;
  });
};

const luuLocalStorage = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();
  var isValid = true;
  var index = danhSachSinhVien.findIndex(function (item) {
    return item.maSv == newSinhVien.maSv;
  });

  var isValidMaSv =
    validatorSv.kiemTraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã sinh viên không được bỏ trống"
    ) && validatorSv.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);

  var isValidTenSv = validatorSv.kiemTraRong(
    "txtTenSV",
    "spanTenSV",
    "Tên sinh viên không được bỏ trống"
  );

  var isValidEmail =
    validatorSv.kiemTraRong(
      "txtEmail",
      "spanEmailSV",
      "Email không được bỏ trống"
    ) && validatorSv.kiemTraEmail("txtEmail", "spanEmailSV");

  isValid = isValidTenSv && isValidMaSv && isValidEmail;

  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);
    resetSinhVien();

    luuLocalStorage();
  }
}

function xoaSinhVien(id) {
  var viTri = timViTri(id, danhSachSinhVien);
  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSinhVien(danhSachSinhVien);
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timViTri(id, danhSachSinhVien);

  var sinhVien = danhSachSinhVien[viTri];
  xuatThongTinLenForm(sinhVien);
}

function updateSinhVien() {
  var sinhVienEdited = layThongTinTuForm();

  var viTri = timViTri(sinhVienEdited.maSv, danhSachSinhVien);
  danhSachSinhVien[viTri] = sinhVienEdited;
  xuatDanhSachSinhVien(danhSachSinhVien);
  resetSinhVien();
  luuLocalStorage();
}

function resetSinhVien() {
  document.getElementById("txtMaSV").value = null;
  document.getElementById("txtTenSV").value = null;
  document.getElementById("txtEmail").value = null;
  document.getElementById("txtDiemToan").value = null;
  document.getElementById("txtDiemLy").value = null;
  document.getElementById("txtDiemHoa").value = null;
}

function searchSinhVien() {
  var searchList = [];
  var tdTag = document.querySelectorAll("#nameTag");
  var searchInput = document.getElementById("txtSearch").value;

  for (var index = 0; index < tdTag.length; index++) {
    if (searchInput == tdTag[index].innerText) {
      searchList.push(danhSachSinhVien[index]);
      xuatDanhSachSinhVien(searchList);
      break;
    } else {
      document.getElementById(
        "tbodySinhVien"
      ).innerHTML = `<p class="text-danger mt-2">Không tìm thấy sinh viên nào</p>`;
    }
  }
}
